### Fix header with 1 row
```javascript
$('table').makeTableScrollable({
    headerNumber: 1,
    borderColor: "#bbb",
    height: 400
})
```
### Fix header with 2 row
```javascript
$('table').makeTableScrollable({
    headerNumber: 2,
    borderColor: "#bbb",
    height: 400
})
```
### Fix footer with 1 row
```javascript
$('table').makeTableScrollable({
    headerNumber: 1,
    footerNumber: 1,
    borderColor: "#bbb",
    height: 400
})
```
### Default options
```javascript
{
    headerNumber: 1,
    footerNumber: 0,
    borderColor: "#bbb",
    height: 400
}
```