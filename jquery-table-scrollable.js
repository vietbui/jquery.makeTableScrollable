﻿/// <reference path="/@JSense.js" />
(function ($) {
    "use strict";

    function getOriginalTable($wrapper) {
        return $wrapper.parent().find('.mts__table table')[0];
    }

    // clone table
    function createElement(name) {
        return document.createElement(name);
    }

    function cloneElementWithoutChild(element) {
        var $newElement = $(createElement(element.tagName));

        $newElement.addClass(element.className);
        $newElement.attr("style", $(element).attr("style") || "");

        return $newElement[0];
    }

    function copyRowsFromOriginalTable(table, rowIndex, rowCount) {
        var $newTable = $(cloneElementWithoutChild(table));

        for (var i = 0; i < rowCount; i++) {
            var $row = $(table.rows[i + rowIndex]).clone();
            $newTable.append($row);
        }

        return $newTable[0];
    }

    // sync size
    function resetWrapperSize($wrapper) {
        var table = $wrapper.find("table")[0];
        var rows = table.rows;

        for (var i = 0; i < rows.length; i++) {
            var row = rows[i];

            for (var j = 0; j < row.cells.length; j++) {
                $(row.cells[j]).css({ height: 'auto', width: 'auto' });
            }
        }
    }

    function syncWrapperSize($wrapper, rowIndex, options) {
        var originalTable = getOriginalTable($wrapper);
        var scrollWidth = $wrapper.parent()[0].offsetWidth - originalTable.offsetWidth;

        $wrapper.css({ right: scrollWidth + 'px' });

        var count = $wrapper.find("tr").length;
        var table = $wrapper.find("table")[0];

        for (var i = 0; i < count; i++) {
            syncTableRowSize(
                originalTable.rows[i + rowIndex].cells,
                table.rows[i].cells,
                options);
        }
    }

    function syncTableRowSize(originalCells, cells, options) {
        for (var i = 0; i < originalCells.length; i++) {
            if (originalCells[i].rowSpan === 1) {
                syncElementSize(originalCells[i], cells[i], "Height", options.important);
            }

            if (originalCells[i].colSpan === 1 && options.freeColumn !== i) {
                syncElementSize(originalCells[i], cells[i], "Width", options.important);
            }
        }
    }

    function syncElementSize(element1, element2, sizeName, important) {
        var offsetName = "offset" + sizeName;
        var size2 = $(element2)[sizeName.toLowerCase()]() + element1[offsetName] - element2[offsetName];

        setElementSize(element2, size2, sizeName, important);

        if (element1[offsetName] !== element2[offsetName]) {
            size2 += (element1[offsetName] - element2[offsetName]);
            setElementSize(element2, size2, sizeName, important);
        }
    }

    function syncTableHeaderSize($headerWrapper, options) {
        resetWrapperSize($headerWrapper);
        syncWrapperSize($headerWrapper, 0, options);
    }

    function syncTableFooterSize($footerWrapper, options) {
        resetWrapperSize($footerWrapper);

        var originalTable = getOriginalTable($footerWrapper);

        syncWrapperSize($footerWrapper, originalTable.rows.length - options.footerNumber, options);
    }

    // set element size
    function setElementSize(element, size, sizeName, important) {
        if (size <= 0) {
            return;
        }

        if (important) {
            setElementSizeImportant(element, size, sizeName);
        } else {
            $(element)[sizeName.toLowerCase()](size);
        }
    }

    function setElementSizeImportant(element, size, sizeName) {
        var $element = $(element);
        var propertyName = sizeName.toLowerCase();
        var cssText = propertyName + ":" + size + "px !important;";
        var style = $element.attr("style") || "";
        var properties = style.split(";");

        for (var i = 0; i < properties.length; i++) {
            var property = properties[i].split(":");

            if (property[0] && $.trim(property[0]).toLowerCase() !== propertyName) {
                cssText += properties[i] + ";";
            }
        }

        $element.attr("style", cssText);
    }

    // plugin
    function makeTableScrollable(table, options) {
        var $table = $(table);
        var header = copyRowsFromOriginalTable(table, 0, options.headerNumber);
        var wrapper = ["<div class='mts' style='position:relative'>",
            "<div class='mts__table' style='transition:height 500ms;overflow-y:scroll;max-height:" +
                options.height + "px;border-bottom:1px solid " + options.borderColor + "'></div>",
            "<div class='mts__header' style='position:absolute;top:0;left:0;'></div>",
            "<div class='mts__footer' style='position:absolute;bottom:0;left:0;'></div>",
        "</div>"].join("");

        var $wrapper = $table.wrap(wrapper).parent();
        var $headerWrapper = $wrapper.next();

        $headerWrapper.append(header);

        if (options.footerNumber) {
            var footerNumber = options.footerNumber;
            var footer = copyRowsFromOriginalTable(table, table.rows.length - footerNumber, footerNumber);
            $headerWrapper.next().append(footer);
        }

        function onResize() {
            syncTableHeaderSize($headerWrapper, options);

            if (options.footerNumber) {
                syncTableFooterSize($headerWrapper.next(), options);
            }
        }

        $(window)
            .off("resize.makeTableScrollable")
            .on("resize.makeTableScrollable", onResize);
        onResize();
    }

    $.fn.makeTableScrollable = function (options) {
        options = $.extend({
            headerNumber: 1,
            borderColor: "#bbb",
            height: 400,
            freeColumn: -1,
            important: false,
            footerNumber: 0
        }, options);
        return this.each(function () {
            makeTableScrollable(this, options);
        });
    };
}(jQuery));